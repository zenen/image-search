import { Find } from "../src/service";
import { findOne } from "../src/server/find";
import { getMockReq, getMockRes } from "@jest-mock/express";

jest.mock("../src/service");

const FindMock = Find as jest.Mock;

describe("find", () => {
  afterEach(jest.clearAllMocks);

  it("undefinedを返されたとき、404ステータスコードを返す.", async () => {
    FindMock.prototype.findOne = jest.fn().mockResolvedValue(undefined);
    const route = findOne(new FindMock());
    const req = getMockReq({ params: { id: "1" } });
    const { next, res } = getMockRes();

    await route(req, res, next);

    expect(res.sendStatus).toHaveBeenCalledWith(404);
  });

  it("正常系.", async () => {
    FindMock.prototype.findOne = jest.fn().mockResolvedValue({});
    const route = findOne(new FindMock());
    const req = getMockReq({ params: { id: "1" } });
    const { res, next } = getMockRes();

    await route(req, res, next);

    expect(res.json).toHaveBeenCalled();
  });
});

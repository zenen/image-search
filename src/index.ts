import { ImageFeatureClient } from "./imageFeature/imageFeatureClient";
import { Milvus } from "./milvus/milvusClient";
import { getApp } from "./server";

const { MILVUS_ADDRESS, IMAGE_FEATURE_ADDRESS, PORT, COLLECTION } = process.env;

const milvus = new Milvus(COLLECTION!, { address: MILVUS_ADDRESS! });
const imageFeature = new ImageFeatureClient({
  address: IMAGE_FEATURE_ADDRESS!,
});

const app = getApp({
  queryService: milvus,
  command: milvus,
  extractor: imageFeature,
});

app.listen(Number(PORT), "0.0.0.0");

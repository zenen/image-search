import { MilvusMigration } from "./milvus/migration";

const collection = process.argv[2];

console.log(collection);

const { MILVUS_ADDRESS } = process.env;

const migration = new MilvusMigration(collection, {
  address: MILVUS_ADDRESS!,
});

migration
  .run()
  .then((_) => console.log("done."))
  .catch(console.error);

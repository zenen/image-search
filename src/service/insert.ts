import { FeatureCommand } from "../core/feature_command";
import { FeatureExtractor } from "../core/feature_extractor";
import { Id } from "../core/id";
import { Image } from "../core/image";

type Dependency = {
  command: FeatureCommand;
  featureExtractor: FeatureExtractor;
};

type Args = {
  id: Id;
  domainId: Id;
  image: Image;
};

export class Insert {
  constructor(private readonly deps: Dependency) {}

  async run(args: Args): Promise<Id> {
    const { id, image, domainId } = args;
    const feature = await this.deps.featureExtractor.getFeature(image);
    return await this.deps.command.insert({
      record: { id, feature, domainId },
    });
  }
}

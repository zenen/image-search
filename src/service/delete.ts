import { FeatureCommand } from "../core/feature_command";
import { Id } from "../core/id";

type Dependency = {
  command: FeatureCommand;
};

type Args = {
  id: Id;
};

export class Delete {
  constructor(private readonly deps: Dependency) {}

  async run(args: Args): Promise<Id> {
    return this.deps.command.delete(args);
  }
}

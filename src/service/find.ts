import { FeatureQuery } from "../core/feature_query";
import { FeatureRecord } from "../core/feature_record";
import { Id } from "../core/id";

type Dependency = {
  queryService: FeatureQuery;
};

type Args = {
  id: Id;
};

export class Find {
  constructor(private readonly deps: Dependency) {}

  async findOne(args: Args): Promise<FeatureRecord | undefined> {
    return await this.deps.queryService.find(args);
  }

  async findByDomain(args: Args): Promise<FeatureRecord[]> {
    return await this.deps.queryService.findByDomain(args);
  }
}

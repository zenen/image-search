import { FeatureQuery, TopKFeatureRecords } from "../core/feature_query";
import { FeatureExtractor } from "../core/feature_extractor";
import { Image } from "../core/image";
import { Feature } from "../core/feature";
import { Id } from "../core/id";

type Dependency = {
  queryService: FeatureQuery;
  featureExtractor: FeatureExtractor;
};

type Args = { topk?: number; domainId?: Id } & (
  | { queryType: "image"; query: Image }
  | { queryType: "feature"; query: Feature }
);

export class Search {
  constructor(private readonly deps: Dependency) {}

  async run(args: Args): Promise<TopKFeatureRecords> {
    const topk = args.topk ?? 5;
    if (args.queryType === "feature") {
      return await this.searchByFeature(args.domainId, args.query, topk);
    }
    const image = args.query;
    const feature = await this.deps.featureExtractor.getFeature(image);
    return await this.searchByFeature(args.domainId, feature, topk);
  }

  async searchByFeature(
    domainId: Id | undefined,
    feature: Feature,
    topk: number
  ): Promise<TopKFeatureRecords> {
    return await this.deps.queryService.search({
      feature,
      topk,
      domainId: domainId,
    });
  }
}

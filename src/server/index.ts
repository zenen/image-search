import express from "express";
import * as formData from "express-form-data";
import { tmpdir } from "os";
import { findByDomain, findOne } from "./find";
import { Find, Insert, Search } from "../service";
import { FeatureQuery } from "../core/feature_query";
import { FeatureCommand } from "../core/feature_command";
import { FeatureExtractor } from "../core/feature_extractor";
import { searchByImage, searchByFeature } from "./search";
import fs from "fs";
import { insert } from "./insert";
import morgan from "morgan";

const imageFileMiddle = (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  const imagePath: string = (req as any)?.files?.file?.path;
  if (!imagePath) {
    res.sendStatus(400);
  } else {
    const image: ArrayBuffer = fs.readFileSync(imagePath);
    res.locals.image = image;
    next();
  }
};

const errorHandler = (
  err: Error,
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  console.error(err.stack);
  res.status(500).json({ err: err.message });
};

export const getApp = (deps: {
  queryService: FeatureQuery;
  command: FeatureCommand;
  extractor: FeatureExtractor;
}) => {
  const app = express();

  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(morgan("common"));
  app.use(formData.parse({ uploadDir: tmpdir(), autoClean: true }));

  app.get("/:id", findOne(new Find({ queryService: deps.queryService })));
  app.get(
    "/domains/:id",
    findByDomain(new Find({ queryService: deps.queryService }))
  );
  app.post(
    "/search",
    imageFileMiddle,
    searchByImage(
      new Search({
        queryService: deps.queryService,
        featureExtractor: deps.extractor,
      })
    )
  );
  app.post(
    "/search/feature",
    searchByFeature(
      new Search({
        queryService: deps.queryService,
        featureExtractor: deps.extractor,
      })
    )
  );
  app.post(
    "/:domainId/:id([0-9]+)",
    imageFileMiddle,
    insert(
      new Insert({ command: deps.command, featureExtractor: deps.extractor })
    )
  );

  app.use(errorHandler);

  return app;
};

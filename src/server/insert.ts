import { Insert } from "../service";
import express from "express";

export const insert = (
  service: Insert
): ((
  req: express.Request,
  resp: express.Response,
  next: express.NextFunction
) => Promise<void>) => {
  return function (req, resp, next) {
    const { id, domainId } = req.params;
    if (!id || !domainId) {
      resp.sendStatus(400);
      return Promise.resolve();
    }
    return service
      .run({ id, image: resp.locals.image, domainId })
      .then((res) => {
        resp.json({ id: res });
      })
      .catch(next);
  };
};

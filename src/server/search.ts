import { Search } from "../service";
import express from "express";

export const searchByImage = (
  service: Search
): ((
  req: express.Request,
  resp: express.Response,
  next: express.NextFunction
) => Promise<void>) => {
  return function (req, resp, next) {
    const topk = req.query.topk as string | undefined;
    const domainId = req.query.domainId as string | undefined;
    return service
      .run({
        queryType: "image",
        query: resp.locals.image,
        topk: Number(topk),
        domainId: domainId,
      })
      .then((records) => {
        resp.json(records);
      })
      .catch(next);
  };
};

export const searchByFeature = (
  service: Search
): ((
  req: express.Request,
  resp: express.Response,
  next: express.NextFunction
) => Promise<void>) => {
  return function (req, resp, next) {
    const topk = req.query.topk as string | undefined;
    const feature = req.body.feature as number[];
    const domainId = req.params.domainId as string;
    if (!feature) {
      resp.sendStatus(400);
      return Promise.resolve();
    }
    return service
      .run({
        queryType: "feature",
        query: feature,
        topk: Number(topk),
        domainId,
      })
      .then((records) => {
        resp.json(records);
      })
      .catch(next);
  };
};

import { Find } from "../service";
import express from "express";

export const findOne = (
  service: Find
): ((
  req: express.Request,
  resp: express.Response,
  next: express.NextFunction
) => Promise<void>) => {
  return function (req, resp, next) {
    const { id } = req.params;
    if (!id) {
      resp.sendStatus(400);
      return Promise.resolve();
    }
    return service
      .findOne({ id })
      .then((record) => {
        if (!record) {
          resp.sendStatus(404);
        } else {
          resp.json(record);
        }
      })
      .catch(next);
  };
};

export const findByDomain = (
  service: Find
): ((
  req: express.Request,
  resp: express.Response,
  next: express.NextFunction
) => Promise<void>) => {
  return function (req, resp, next) {
    const { id } = req.params;
    if (!id) {
      resp.sendStatus(400);
      return Promise.resolve();
    }
    return service
      .findByDomain({ id })
      .then((record) => {
        resp.json(record);
      })
      .catch(next);
  };
};

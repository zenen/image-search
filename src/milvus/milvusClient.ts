import {
  DeleteArgs,
  FeatureCommand,
  InsertArgs,
} from "../core/feature_command";
import {
  FeatureQuery,
  FindByDomainQuery,
  FindQuery,
  SearchQuery,
  TopKFeatureRecords,
} from "../core/feature_query";
import { FeatureRecord } from "../core/feature_record";
import { Id } from "../core/id";
import {
  ClientConfig,
  LoadState,
  MilvusClient,
} from "@zilliz/milvus2-sdk-node/dist/milvus";
import { throwIfError } from "./error";

export class Milvus implements FeatureQuery, FeatureCommand {
  private readonly client: MilvusClient;

  constructor(private readonly collection: string, config: ClientConfig) {
    this.client = new MilvusClient(config);
  }

  async insert(args: InsertArgs): Promise<Id> {
    if (await this.isExists(args.record.id as number)) {
      throw new Error(`id "${args.record.id}" is already exists.`);
    }
    const { status } = await this.client.insert({
      collection_name: this.collection,
      fields_data: [
        {
          id: args.record.id,
          feature: args.record.feature as number[],
          domain_id: args.record.domainId,
        },
      ],
    });
    throwIfError(status);
    return args.record.id;
  }

  async delete(args: DeleteArgs): Promise<Id> {
    const { status } = await this.client.deleteEntities({
      collection_name: this.collection,
      expr: `id in [${args.id as number}]`,
    });
    throwIfError(status);
    return args.id;
  }

  async find(query: FindQuery): Promise<FeatureRecord | undefined> {
    await this.load(this.collection);
    const { data, status } = await this.client.query({
      collection_name: this.collection,
      expr: `id in [${query.id}]`,
      output_fields: ["id", "feature", "domain_id"],
    });
    throwIfError(status);
    if (data.length < 1) {
      return undefined;
    }
    return data[0] as FeatureRecord;
  }

  async findByDomain(query: FindByDomainQuery): Promise<FeatureRecord[]> {
    await this.load(this.collection);
    const { data, status } = await this.client.query({
      collection_name: this.collection,
      expr: `domain_id in ["${query.id}"]`,
      output_fields: ["id", "feature", "domain_id"],
    });
    throwIfError(status);
    return data as FeatureRecord[];
  }

  async search(query: SearchQuery): Promise<TopKFeatureRecords> {
    await this.load(this.collection);
    const expr =
      query.domainId !== undefined
        ? `domain_id in ["${query.domainId}"]`
        : undefined;
    const { results, status } = await this.client.search({
      collection_name: this.collection,
      vector: query.feature as number[],
      topk: query.topk,
      expr,
    });
    throwIfError(status);
    return Promise.all(
      results.map(async (item) => ({
        similarity: item.score,
        record: (await this.find({ id: item.id }))!,
      }))
    );
  }

  private async load(collection: string) {
    const isLoaded = !(await this.isNotLoaded(collection));
    if (isLoaded) return;
    const status = await this.client.loadCollectionSync({
      collection_name: collection,
    });
    throwIfError(status);
  }

  private async isNotLoaded(collection: string): Promise<boolean> {
    const { status, state } = await this.client.getLoadState({
      collection_name: collection,
    });
    throwIfError(status);
    return state === LoadState.LoadStateNotLoad;
  }

  private async isExists(id: number): Promise<boolean> {
    const res = await this.find({ id });
    return res != undefined;
  }
}

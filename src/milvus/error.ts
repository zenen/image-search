export function throwIfError(status: {
  error_code: string | number;
  reason: string;
}) {
  if (status.error_code !== "Success") throw new Error(status.reason);
}

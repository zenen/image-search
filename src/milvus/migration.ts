import {
  ClientConfig,
  DataType,
  MilvusClient,
} from "@zilliz/milvus2-sdk-node/dist/milvus";
import { throwIfError } from "./error";

const schema = [
  {
    name: `id`,
    description: ``,
    data_type: DataType.Int64,
    is_primary_key: true,
    auto_id: false,
  },
  {
    name: `domain_id`,
    description: ``,
    data_type: DataType.VarChar,
    max_length: 255,
  },
  {
    name: `feature`,
    description: ``,
    data_type: DataType.FloatVector,
    dim: 512,
  },
];

export class MilvusMigration {
  private readonly collection: string;
  private readonly client: MilvusClient;

  constructor(collection: string, config: ClientConfig) {
    this.collection = collection;
    this.client = new MilvusClient(config);
  }

  async createCollection() {
    const status = await this.client.createCollection({
      collection_name: this.collection,
      description: ``,
      fields: schema,
    });
    throwIfError(status);
  }

  async createIndex() {
    const status = await this.client.createIndex({
      collection_name: this.collection,
      field_name: `feature`,
      extra_params: {
        metric_type: "L2",
        index_type: "IVF_FLAT",
        params: JSON.stringify({ nlist: 1024 }),
      },
    });
    throwIfError(status);
  }

  async run() {
    await this.createCollection();
    await this.createIndex();
  }
}

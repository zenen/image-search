import { Feature } from "../core/feature";
import { FeatureExtractor } from "../core/feature_extractor";

const MODEL = "vgg";

type Config = {
  address: string;
};

export class ImageFeatureClient implements FeatureExtractor {
  constructor(private readonly config: Config) {}

  async getFeature(image: ArrayBuffer): Promise<Feature> {
    const form = new FormData();
    form.append("file", new Blob([image]));
    const resp = await fetch(`${this.config.address}/${MODEL}/feature`, {
      method: "POST",
      body: form,
    });
    if (!resp.ok) {
      throw new Error(resp.statusText);
    }
    const respJson = await resp.json();
    return respJson.feature as number[];
  }
}

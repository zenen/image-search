import { Id } from "./id";
import { Feature } from "./feature";
import { FeatureRecord } from "./feature_record";

export type FindQuery = {
  id: Id;
};

export type FindByDomainQuery = {
  id: Id;
};

export type SearchQuery = {
  feature: Feature;
  domainId: Id | undefined;
  topk: number;
};

export type TopKFeatureRecords = {
  record: FeatureRecord;
  similarity: number;
}[];

export interface FeatureQuery {
  find(query: FindQuery): Promise<FeatureRecord | undefined>;
  findByDomain(query: FindByDomainQuery): Promise<FeatureRecord[]>;
  search(query: SearchQuery): Promise<TopKFeatureRecords>;
}

import { Id } from "./id";
import { Feature } from "./feature";

export type FeatureRecord = {
  id: Id;
  domainId: Id;
  feature: Feature;
};

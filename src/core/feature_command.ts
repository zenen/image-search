import { Id } from "./id";
import { FeatureRecord } from "./feature_record";

export type InsertArgs = {
  record: FeatureRecord;
};

export type DeleteArgs = {
  id: Id;
};

export interface FeatureCommand {
  insert(args: InsertArgs): Promise<Id>;
  delete(args: DeleteArgs): Promise<Id>;
}

import { Feature } from "./feature";
import { Image } from "./image";

export interface FeatureExtractor {
  getFeature(image: Image): Promise<Feature>;
}
